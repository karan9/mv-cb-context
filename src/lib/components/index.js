import React from "react";

const CB_CONSTANTS = {
  VISBILITY: {
    EXPAND: "EXPAND",
    MAXIMIZE: "MAXIMIZE",
    MINIMIZE: "MINIMIZE"
  }
};

const ChatbotContext = new React.createContext({
  cb: {
    visiblity: CB_CONSTANTS.VISBILITY.EXPAND
  },
  actions: {
    onMinimize: () => {
      console.log("Minimize");
    },
    onMaximize: () => {
      console.log("Maximize");
    },
    onExpand: () => {
      console.log("Expand");
    }
  }
});

export class CBContextProvider extends React.Component {
  constructor(props) {
    super(props);

    this.onMinimize = () => {
      alert("Minimize");
    };

    this.onMaximize = () => {
      alert("Maximize");
    };

    this.onExpand = () => {
      alert("Expand");
    };

    this.state = {
      cb: {
        visiblity: CB_CONSTANTS.VISBILITY.EXPAND
      },
      actions: {
        onMinimize: this.onMinimize,
        onMaximize: this.onMaximize,
        onExpand: this.onExpand
      }
    };
  }

  render() {
    return (
      <ChatbotContext.Provider value={this.state}>
        {this.props.children}
      </ChatbotContext.Provider>
    );
  }
}

export function withChatbotContext(WrappedComponent) {
  return class extends React.Component {
    render() {
      return (
        <ChatbotContext.Consumer>
          {value => {
            return <WrappedComponent cb={value.cb} actions={value.actions} />;
          }}
        </ChatbotContext.Consumer>
      );
    }
  };
}
